#! /bin/sh
. ./.env
UNAME=$(uname)
if [[ $UNAME == "Darwin" ]]; then
    TAR=gtar
else
    TAR=tar
fi
docker-compose build
docker save redmine4_backlogs:${TAG} | gzip > redmine4_backlogs-image-${TAG}.tgz
$TAR cfz redmine4-backlogs-release-${TAG}.tgz --transform="s|^|redmine4_backlogs-${TAG}/|" docker-compose.yml README.md plugin_install.sh .env init.sh docker-compose-init.yml redmine4_backlogs-image-${TAG}.tgz Vagrantfile
echo redmine4-backlogs-release-${TAG}.tgz
