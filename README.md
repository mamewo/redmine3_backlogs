Redmine as docker container
===========================

Prepare
-------
1. run

```
init.sh
```

2. load default settings of redmine
  1. open redmine http://localhost:3000/redmine
  2. login as admin/admin
  3. click Administration -> load default setting -> select English

Run
---
1. run docker container

```
docker-compose up -d
```

Stop
-----

```
docker-compose stop
```

Configuration to change
-----------------------
1. markdown
2. farend fancy theme
3. define workflow of task_tracker
4. Mail server setting
5. Host name, port and path

Mail server settings
-------------------
1. edit config/configuration.yml in redmine container
e.g. gmail

```
default:
  # Outgoing emails configuration
  # See the examples below and the Rails guide for more configuration options:
  # http://guides.rubyonrails.org/action_mailer_basics.html#action-mailer-configuration

  email_delivery:
    delivery_method: :smtp
    smtp_settings:
      enable_starttls_auto: true
      address: "smtp.gmail.com"
      port: 587
      domain: "smtp.gmail.com" # 'your.domain.com' for GoogleApps
      authentication: :plain
      user_name: "your_username@gmail.com"
      # google app password genereted by following web page
      # https://security.google.com/settings/security/apppasswords
      password: "xxxxxxxxxxxx"
```


Installed plugins, preferences
------------------------------
* [redmine_tagging](https://github.com/Restream/redmine_tagging.git)
* [sidebar_hide](https://github.com/bdemirkir/sidebar_hide.git)
* header color: `redmine/color.patch`
* url prefix is /redmine
* [Redmine Issue Templates Plugin](https://github.com/akiko-pusu/redmine_issue_templates.git)
* show project name to auto complete of related ticket

Hardware/OS which I tried
---------------------------
* Ubuntu 16.04, 18.04, 20.04
* Mac book pro 2008 (Docker Desktop)
* Raspberry Pi2 Model B
  * ARMv7 (32bit)
  * 1 GB RAM
* aws t2.small

Additional
----------
### Redmine Checklists plugin by RedmineUP
1. copy plugin directory as lib/redmine/plugin/redmine_plugin directory
2. run

```
docker-compose exec redmine bundle exec rake db:migrate RAILS_ENV=production
```

Detail of init.sh
-------------------
1. initialize db with redmine:4.1.1 without plugins
    * to create redmine tables
2. initialize db and redmine container with plugins

### Remine google analytics plugin
https://github.com/edavis10/redmine-google-analytics-plugin

Reference
---------
* python-mechanize: http://mechanize.readthedocs.io/en/latest/index.html
* mechanize http://wwwsearch.sourceforge.net/mechanize/

----
Takashi Masuyama < mamewotoko@gmail.com >
http://mamewo.ddo.jp/
