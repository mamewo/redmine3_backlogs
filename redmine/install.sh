#! /bin/sh

cd plugins
gem install holidays --version 1.0.3
#bundle update
bundle install
export RAILS_ENV=production

#if [ "$RESTORE_MODE" != t ]; then
bundle exec rake acts_as_taggable_on_engine:install:migrations RAILS_ENV=production
# bundle rake acts_as_taggable_on_engine:install:migrations
#fi
bundle exec rake db:migrate RAILS_ENV=production
bundle exec rake redmine:plugins:migrate RAILS_ENV=production

## TODO; support data migration
# bundle exec rake redmine:backlogs:install RAILS_ENV=production <<EOF
# 1 2 3
# y
# task_tracker
# y
# EOF
