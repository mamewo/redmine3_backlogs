#! /bin/bash
set -ex
TOPDIR=$(pwd)
# export USER_ID=$(id -u)
# postgres
DOCKER_COMPOSE_YML=${1:-docker-compose.yml}
PORT=${PORT:-3210}
PREFIX=${PREFIX:-/redmine_test}
RESTORE_MODE=${RESTORE_MODE:-t}

export REDMINE_PREFIX=$PREFIX
export REDMINE_PORT=$PORT
export RESTORE_MODE=$RESTORE_MODE

echo $DOCKER_COMPOSE_YML $PORT $PREFIX
docker-compose -f $DOCKER_COMPOSE_YML down || true
sudo rm -rf volume
docker-compose -f $DOCKER_COMPOSE_YML build
docker-compose -f $DOCKER_COMPOSE_YML up -d db

if [ "$DOCKER_COMPOSE_YML" = docker-compose.yml ]; then
    # postgresql
    LOGLINE="database system is ready to accept connections"
    REDIME_LOGLINE="ActiveSupport is required for"
else
    # mariadb
    LOGLINE="mariadb.org binary distribution"
    if [ "$RESTORE_MODE" = t ]; then
        REDMINE_LOGLINE='ActiveSupport is required for TimeWithZone support'
    else
        REDMINE_LOGLINE='AddRolesSettings: migrated'
    fi
fi

grep -m1 "$LOGLINE" <( docker-compose -f $DOCKER_COMPOSE_YML logs -f db )

sudo mkdir -p volume/redmine/log
if [ "$RESTORE_MODE" = t ]; then
    # restore backup data
    if [ "$DOCKER_COMPOSE_YML" = docker-compose.yml ]; then
        # postgresql
        sudo mkdir -p volume/postgres
        sudo tar xfvz data/postgres/volume_postgresql.tar.gz volume/postgres/redmine_pgsql.sql
        # sudo cp volume/postgres/redmine_pgsql.sql volume/postgres/
        POSTGRES_PASSWORD=example docker-compose exec -T db sh -c "psql -U postgres postgres < /var/lib/postgresql/data/redmine_pgsql.sql"    
    else
        sudo mkdir -p volume/mariadb
        sudo chown -R 999:999 volume
        sh restore.sh
    fi
fi

docker-compose -f $DOCKER_COMPOSE_YML up -d redmine
# TODO: wait by log
( cd test/mechanize/; docker-compose build )
cat > test/mechanize/.secret.json <<EOF
{
    "username": "admin",
    "password": "adminadmin",
    "redmine_url": "http://localhost:${PORT}${PREFIX}",
    "project_name": "test_project"
}
EOF
cat test/mechanize/.secret.json

grep -m1 "$REDMINE_LOGLINE" <(docker-compose -f $DOCKER_COMPOSE_YML logs -f redmine)

if [ "$RESTORE_MODE" != t ]; then
    sleep 30
    ( cd test/mechanize/; docker-compose run --rm mechanize_tester ./init_login.py )
    ( cd test/mechanize/; docker-compose run --rm mechanize_tester ./init_config.py )
fi

docker-compose -f $DOCKER_COMPOSE_YML exec -T redmine env RESTORE_MODE=$RESTORE_MODE bash -x /etc/install.sh
docker-compose -f $DOCKER_COMPOSE_YML restart
grep -m1 'Bundled gems are installed into' <( docker-compose -f $DOCKER_COMPOSE_YML logs -f redmine )

if [ "$RESTORE_MODE" != t ]; then
    sleep 60    
    ( cd test/mechanize/; docker-compose run --rm mechanize_tester ./add_task.py )
fi

rm -rf result || true
mkdir result

# docker file lint
docker run --rm -i hadolint/hadolint < redmine/Dockerfile > result/redmine-dockerfile-checkresult.txt
docker run --rm -i hadolint/hadolint < ./test/mechanize/Dockerfile > result/mechanize-dockerfile-checkresult.txt
