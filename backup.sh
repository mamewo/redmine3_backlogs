#! /bin/sh

DB_USERNAME=root
DB_PASSWORD=example
DB_NAME=redmine

# redmine files
sudo tar cfz volume.tar.gz volume

# mariadb
docker-compose exec db mysqldump -u $DB_USERNAME -p$DB_PASSWORD $DB_NAME > redmine_mariadb.sql
