#! /usr/bin/env python2
# -*- coding: utf-8 -*-
import mechanize
import sys
import json
import traceback

SECRET_DATA = None


def init_login():
    br = mechanize.Browser()
    try:
        br.open(SECRET_DATA['redmine_url'])
        br.follow_link(text="Sign in")
        br.select_form(nr=2)
        # init password
        br['username'] = 'admin'
        br['password'] = 'admin'
        br.submit()

        # change password
        br.select_form(nr=2)
        br['password'] = 'admin'
        br['new_password'] = SECRET_DATA['password']
        br['new_password_confirmation'] = SECRET_DATA['password']
        br.submit()
        
        # load english default settings        
        br.follow_link(text="Administration")
        br.select_form(nr=2)
        # default is english
        br.submit()

    except:
        traceback.print_exc()
        print("***************")
        print(br.response().read())
        raise
        

if __name__ == '__main__':
    with open('.secret.json') as f:
        SECRET_DATA = json.load(f)

    init_login()
