#! /usr/bin/env python2
# -*- coding: utf-8 -*-
import mechanize
import sys
import json
import traceback

SECRET_DATA = None

# 1. markdown
# 2. farend fancy theme
# - 3. define workflow of task_tracker
# - 4. Mail server setting
# - 5. Host name, port and path
# 6. show new issue tab
# 7. select default tracker
def init_config():
    
    br = mechanize.Browser()
    try:
        base_index = 2
        general_form_index = base_index
        display_form_index = base_index + 1
        auth_form_index = base_index + 2
        api_form_index = base_index + 3
        project_form_index = base_index + 4
        issues_form_index = base_index + 5
        attachment_form_index = base_index + 6
        mail_form_index = base_index + 7
        repository_form_index = base_index + 8

        br.open(SECRET_DATA['redmine_url'])
        br.follow_link(text="Sign in")
        br.select_form(nr=2)
        # init password
        br['username'] = SECRET_DATA['username']
        br['password'] = SECRET_DATA['password']
        br.submit()

        br.follow_link(text="Administration")
        br.follow_link(text="Settings")
        br.select_form(nr=general_form_index)
        br['settings[text_formatting]'] = ['markdown']
        br.submit()
        
        br.select_form(nr=display_form_index)
        # Display the "New issue" tab
        br['settings[new_item_menu_tab]'] = ['1']
        br['settings[ui_theme]'] = ['farend_fancy']
        br.submit()
      
        br.select_form(nr=project_form_index)
        br.form.find_control(name='settings[default_projects_public]', type='checkbox').items[0].selected = False
        br.submit()
        
        if SECRET_DATA.get('use_api'):
            br.select_form(nr=api_form_index)
            br.form.find_control(name='settings[rest_api_enabled]', type='checkbox').items[0].selected = True
            br.submit()
    except:
        traceback.print_exc()
        print("***************")
        print(br.response().read())
        raise


if __name__ == '__main__':
    with open('.secret.json') as f:
        SECRET_DATA = json.load(f)

    init_config()
