#! /usr/bin/env python2
# -*- coding: utf-8 -*-
import mechanize
import json
import sys
import traceback

SECRET_DATA=None


def add_project():
    br = mechanize.Browser()
    try:
        br.open(SECRET_DATA['redmine_url'])

        br.follow_link(text="Sign in")
        br.select_form(nr=2)
        br['username'] = SECRET_DATA['username']
        br['password'] = SECRET_DATA['password']
        br.submit()

        br.follow_link(text="Projects")
        br.follow_link(text="New project")

        br.select_form(nr=2)
        br.set_value(SECRET_DATA['project_name'], id="project_name")
        br.set_value(SECRET_DATA['project_name'], id="project_identifier")
        br.submit()

        base_index = 2
        information_form_index = base_index
        module_form_index = base_index + 1

        br.select_form(nr=information_form_index)
        
        br.form.find_control(type="checkbox", name='project[tracker_ids][]').get('1').selected = True
        br.form.find_control(type="checkbox", name='project[tracker_ids][]').get('2').selected = True
        br.form.find_control(type="checkbox", name='project[tracker_ids][]').get('3').selected = True
        br.form.find_control(type="checkbox", name='project[tracker_ids][]').get('4').selected = True

        # for control in br.form.controls:
        #     print('%s %s' % (control.type, control.name))

        br.select_form(nr=module_form_index)
        # br.form.find_control(type="checkbox", name='project[enabled_module_names][]').get('backlogs').selected = True
        # br.form.find_control(type="checkbox", name='project[enabled_module_names][]').get('glossary').selected = True
        br.form.find_control(type="checkbox", name='enabled_module_names[]').get('backlogs').selected = True
        br.form.find_control(type="checkbox", name='enabled_module_names[]').get('glossary').selected = True
        # br.form.find_control(type="checkbox", name='project[enabled_module_names][]').get('issue_templates').selected = True
        br.submit()
    except:
        traceback.print_exc()
        print("***************")
        print(br.response().read())
        raise


def add_task():
    br = mechanize.Browser()
    try:
        br.open(SECRET_DATA['redmine_url'])

        br.follow_link(text="Sign in")
        br.select_form(nr=2)
        br['username'] = SECRET_DATA['username']
        br['password'] = SECRET_DATA['password']
        br.submit()

        br.follow_link(text='Projects')
        br.follow_link(text=SECRET_DATA['project_name'])
        br.follow_link(text="New issue")

        br.select_form(nr=2)
        br.set_value(u"test", id="issue_subject")
        br.submit()
    except:
        traceback.print_exc()
        print("***************")
        print(br.response().read())
        raise


if __name__ == '__main__':
    with open('.secret.json') as f:
        SECRET_DATA = json.load(f)
    add_project()
    # add_task()
