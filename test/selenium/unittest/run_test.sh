#! /bin/sh
rm -rf result screenshot
mkdir result
#export REDMINE_USERNAME=
#export REDMINE_PASSWORD=
#export REDMINE_PROJECT_NAME=
#export REDMINE_URL=
HEADLESS=yes

if [ -f ../env ]; then
    . ../env
fi

#TARGET=test_redmine.TestRedmineBrowseCase.test_login
TARGET=test_redmine

UNAME=$(uname -s)
PREFIX_COMMAND=

if [ "$UNAME" = "Linux" ] && [ "$HEADLESS" = "yes" ]; then
    PYTHONPATH=./libs/ParametrizedTestCase/:./libs/HTMLTestRunner:$PYTHONPATH $PREFIX_COMMAND xvfb-run -a --server-args="-screen 0 1024x800x8" ./test_redmine.py --output=result/report.html
else
    PYTHONPATH=./libs/ParametrizedTestCase/:./libs/HTMLTestRunner:$PYTHONPATH $PREFIX_COMMAND ./test_redmine.py --output=result/report.html
fi
# mobile ui
#PYTHONPATH=./libs/ParametrizedTestCase/:./libs/HTMLTestRunner:$PYTHONPATH ./test_redmine.py --output=result/report.html
