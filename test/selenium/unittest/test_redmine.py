#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import json
import os
import os.path
import shutil
import sys
import time
import traceback
import unittest
from unittest import TestSuite
from urllib.parse import urljoin
from ParametrizedTestCase import ParametrizedTestCase
from HTMLTestRunner import HTMLTestRunner, HTMLTestCase

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import logging

log = logging.getLogger('test.redmine')

SCREENSHOT_DIRNAME = 'screenshot'
SCREENSHOT_DIR = None

# prepare .secret.json like below
#
# { 'username': 'xxx',
#   'password': 'xxx',
#   'redmine_url_base': 'xxx'
#   'project_name': 'xxx' }

USERNAME = None
PASSWORD = None
REDMINE_URL_BASE = None
PROJECT_NAME = None


class WebTestCase(HTMLTestCase, ParametrizedTestCase):
    """ browse page and take screenshot."""

    def setUp(self):
        default_user_agent = "Mozilla/5.0 (Linux; Android 4.1.2; SHL21 Build/S4011) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.83 Mobile Safari/537.36"
        default_dim = (1024, 800)
        value = self.param['user_agent']
        if value is not None:
            user_agent, dim = value
        else:
            user_agent = default_user_agent
            dim = default_dim

        opts = Options()
        opts.add_argument("user-agent=" + user_agent)
        opts.add_argument("window-size=" + ",".join(map(str, dim)))
        self.driver = webdriver.Chrome(chrome_options=opts)
        self.screenshot = []

    def tearDown(self):
        self.driver.close()

    def save_screenshot(self, filename):
        time_prefix = int(time.time())
        htmlpath = "%s/%d_%s" % (SCREENSHOT_DIRNAME, time_prefix, filename)
        ospath = "%s/%d_%s" % (SCREENSHOT_DIR, time_prefix, filename)
        self.driver.save_screenshot(ospath)
        self.screenshot.append({'path': htmlpath, 'url': self.driver.current_url})

    def desc_html(self):
        # + str(self.param)
        return "<br/>"

    def result_html(self):
        if not hasattr(self, 'screenshot'):
            return "no screenshot"

        img = ""
        for screen in self.screenshot:
            img += """<div><a href="%s">%s</a><br>
<img class="screenshot" src="%s" /></div> """ % (screen['url'], screen['url'], screen['path'])
        return img


class TestRedmineBrowseCase(WebTestCase):
    """ Redmine test for desktop layout"""

    def setUp(self):
        default_user_agent = "Mozilla/5.0 (Linux; Android 4.1.2; SHL21 Build/S4011) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.83 Mobile Safari/537.36"
        default_dim = (1024, 800)
        self.REDMINE_SUBDIR = self.param['subdir']
        value = self.param['user_agent']
        if value is not None:
            user_agent, dim = value
        else:
            user_agent = default_user_agent
            dim = default_dim

        opts = Options()
        opts.add_argument("user-agent=" + user_agent)
        opts.add_argument("window-size=" + ",".join(map(str, dim)))
        self.driver = webdriver.Chrome(options=opts)
        self.screenshot = []

    def tearDown(self):
        self.driver.close()
        self.driver.quit()

    def save_screenshot(self, name=None):
        if name is None:
            name = self._testMethodName
        filename = name + '.png'
        time_prefix = int(time.time())
        htmlpath = "%s/%d_%s" % (SCREENSHOT_DIRNAME, time_prefix, filename)
        ospath = "%s/%d_%s" % (SCREENSHOT_DIR, time_prefix, filename)
        self.driver.save_screenshot(ospath)
        self.screenshot.append({'path': htmlpath,
                                'url': self.driver.current_url})
        log.info("""<div><a href="{1}">{1}</a><br><a href="{0}"><img class="screen_capture" src="{0}"></a></div>""".format(htmlpath, self.driver.current_url))

    def get_page(self, path):
        global REDMINE_URL_BASE
        url = urljoin(REDMINE_URL_BASE, self.REDMINE_SUBDIR + '/')
        url = urljoin(url, path)
        log.info(url)
        self.driver.get(url)
        time.sleep(3)      
        
    def login(self):
        self.get_page('login')
        self.save_screenshot()

        login_link = self.driver.find_element_by_xpath('//*[@id="account"]/ul/li[1]/a')
        login_link.click()
        time.sleep(3)
        username_text = self.driver.find_element_by_xpath('//*[@id="username"]')
        username_text.click()
        username_text.clear()
        username_text.send_keys(USERNAME)
        password_text = self.driver.find_element_by_xpath('//*[@id="password"]')
        password_text.click()
        password_text.clear()
        password_text.send_keys(PASSWORD)

        login_button = self.driver.find_element_by_xpath('//*[@id="login-submit"]')
        login_button.click()
        time.sleep(3)

        self.save_screenshot()

    def test_login(self):
        self.login()
        # TODO: add assert

    def test_admin_users(self):
        self.login()
        self.get_page('users')
        self.save_screenshot()

    def test_activity(self):
        self.login()
        self.get_page('projects/%s/activity' % PROJECT_NAME)
        self.save_screenshot()

    def test_roadmap(self):
        self.login()
        self.get_page('projects/%s/roadmap' % PROJECT_NAME)
        self.save_screenshot()
        
    def test_backlog(self):
        self.login()
        self.get_page('rb/master_backlog/%s' % PROJECT_NAME)
        self.save_screenshot()

    def test_release(self):
        self.login()
        self.get_page('rb/releases/%s' % PROJECT_NAME)
        self.save_screenshot()

    def test_ticket(self):
        self.login()
        self.get_page('projects/%s/issues' % PROJECT_NAME)
        self.save_screenshot()
        
    def test_new_ticket(self):
        self.login()
        self.get_page('projects/%s/issues/new' % PROJECT_NAME)
        self.save_screenshot()

    def test_time_entries(self):
        self.login()
        self.get_page('projects/%s/time_entries' % PROJECT_NAME)
        self.save_screenshot()
        
    def test_gantt(self):
        self.login()
        self.get_page('projects/%s/issues/gantt' % PROJECT_NAME)
        self.save_screenshot()

    def test_calendar(self):
        self.login()
        self.get_page('projects/%s/issues/calendar' % PROJECT_NAME)
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_news(self):
        self.login()
        self.get_page('projects/%s/news' % PROJECT_NAME)
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_documents(self):
        self.login()
        self.get_page('projects/%s/documents' % PROJECT_NAME)
        self.save_screenshot()

    def test_wiki(self):
        self.login()
        self.get_page('projects/%s/wiki' % PROJECT_NAME)
        # self.save_screenshot()
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_files(self):
        self.login()
        self.get_page('projects/%s/files' % PROJECT_NAME)
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_glossary(self):
        self.login()
        self.get_page('projects/%s/glossary' % PROJECT_NAME)
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_setting(self):
        self.login()
        self.get_page('projects/%s/settings' % PROJECT_NAME)
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_setting_modules(self):
        self.login()
        self.get_page('projects/%s/settings/modules' % PROJECT_NAME)
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_setting_members(self):
        self.login()
        self.get_page('projects/%s/settings/members' % PROJECT_NAME)
        self.save_screenshot()

    def test_admin_projects(self):
        self.login()
        self.get_page('admin/projects')
        self.save_screenshot()

    def test_admin_groups(self):
        self.login()
        self.get_page('groups')
        self.save_screenshot()

    def test_admin_roles(self):
        self.login()
        self.get_page('roles')
        self.save_screenshot()

    def test_admin_trackers(self):
        self.login()
        self.get_page('trackers')
        self.save_screenshot()

    def test_admin_ticket_statuses(self):
        self.login()
        self.get_page('issue_statuses')
        self.save_screenshot()

    def test_admin_kanban(self):
        self.login()
        self.get_page('rb/taskboards/5')
        self.save_screenshot()

    def test_admin_workflows(self):
        self.login()
        self.get_page('workflows/edit')
        self.save_screenshot()

    def test_admin_custom_fields(self):
        self.login()
        self.get_page('custom_fields')
        self.save_screenshot()

    def test_admin_enumerations(self):
        self.login()
        self.get_page('enumerations')
        self.save_screenshot()

    def test_admin_settings(self):
        self.login()
        self.get_page('settings')
        self.save_screenshot()
        # TODO: test tabs (display...)

    def test_admin_ldap(self):
        self.login()
        self.get_page('auth_sources')
        self.save_screenshot()

    def test_admin_global_issue_templates(self):
        self.login()
        self.get_page('global_issue_templates')
        self.save_screenshot()

    def test_admin_version(self):
        self.login()
        self.get_page('admin/info')
        self.save_screenshot()

    def test_admin_plugin(self):
        self.login()
        self.get_page('admin/plugins')
        self.save_screenshot()

    def test_my_page(self):
        self.login()
        self.get_page('my/page')
        self.save_screenshot()

    def test_private_config(self):
        self.login()
        self.get_page('my/account')
        self.save_screenshot()

    def test_first_ticket(self):
        self.login()
        self.get_page('issues/1')
        self.save_screenshot()


def print_usage():
    print("REDMINE_URL= REDMINE_PASSWORD= REDMINE_URL= REDMINE_PROJECT_NAME= %s [--output] [--title]" % sys.argv[0])


if __name__ == "__main__":   
    parser = argparse.ArgumentParser()
    parser.add_argument('--output')
    parser.add_argument('--title')
    args = parser.parse_args()
    
    if args.output is None:
        filename = 'report.html'
    else:
        filename = args.output

    if args.title is None:
        title = 'Web Test'
    else:
        title = args.title
    output_dir = os.path.dirname(filename)
    SCREENSHOT_DIR = os.path.join(output_dir, SCREENSHOT_DIRNAME)

    user_agent_list = [
        ("Mozilla/5.0 (Linux; Android 4.1.2; SHL21 Build/S4011) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.83 Mobile Safari/537.36", (1024, 800)),
        # ("Mozilla/5.0 (Linux; Android 4.1.2; SHL21 Build/S4011) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.83 Mobile Safari/537.36", (800, 1024))
    ]
    SUBDIR = os.environ.get('RESULT_NAME', 'redmine')
    # subdir_list = [
    #     'redmine',
    #     'redmine_gitmike',
    #     'redmine_modula_gitlab',
    #     'redmine_themes',
    #     'redmine_classic',
    #     'redmine_default'
    # ]
    subdir_list = [SUBDIR]

    try:
        if output_dir not in ['.', ''] and os.path.exists(output_dir):
            shutil.rmtree(output_dir)
        os.makedirs(SCREENSHOT_DIR)

        config_file = '.secret.json'

        if os.path.exists(config_file):
            with open(config_file, 'r') as f:
                data = json.load(f)
                USERNAME = data['username']
                PASSWORD = data['password']
                REDMINE_URL_BASE = data['redmine_url_base']
                PROJECT_NAME = data['project_name']
        else:
            USERNAME = os.environ.get('REDMINE_USERNAME')
            PASSWORD = os.environ.get('REDMINE_PASSWORD')
            REDMINE_URL_BASE = os.environ.get('REDMINE_URL_BASE')
            PROJECT_NAME = os.environ.get('REDMINE_PROJECT_NAME')

        if None in [USERNAME, PASSWORD, REDMINE_URL_BASE, PROJECT_NAME]:
            print_usage()
            sys.exit(1)
            
        all_suite = TestSuite()
        # TODO; create parameter generator (e.g. cartesian product)
        for user_agent in user_agent_list:
            for subdir in subdir_list:
                # TODO: generate param and test lazy
                param = {'user_agent': user_agent, 'subdir': subdir}
                all_suite.addTest(ParametrizedTestCase.parametrize(TestRedmineBrowseCase, param=param))

        # unittest
        with open(filename, "wb") as output:
            runner = HTMLTestRunner(stream=output,
                                    verbosity=1,
                                    title=title,
                                    show_mode=HTMLTestRunner.SHOW_ALL)
            runner.run(all_suite)
        print(filename)
    except Exception:
        print(traceback.format_exc())
        if os.path.exists(filename):
            os.remove(filename)
