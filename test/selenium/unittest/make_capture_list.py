#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re

OUTFILE = 'js/data.js'

if __name__ == "__main__":
    mypath = 'result/screenshot/'
    lst = [os.path.join(mypath, f) for f in os.listdir(mypath) if not re.search('login', f) and os.path.isfile(os.path.join(mypath, f))]
    with open(OUTFILE, 'w') as f:
        f.write("var imagelist = [\n")
        for filepath in lst:
            f.write('"%s",\n' % filepath)
        f.write("];\n")

