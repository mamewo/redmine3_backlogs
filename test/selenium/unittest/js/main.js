var n = imagelist.length;
//data
var image_width = 1004;
var image_height = 675;
var theta = 2*Math.PI/n;
var theta_offset = -Math.PI/2;
var w = image_width/2;
var h = image_height/2;
// var w = image_width/2;
// var h = image_height/2;
var r = w/(2*Math.sin(theta/2));
console.log('r %f', r);
console.log('n %d', n);

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 3*r);

var renderer = new THREE.WebGLRenderer({antialias:true});
renderer.setSize( window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
var control = new THREE.OrbitControls(camera, renderer.domElement);

var light = null;
// var light = new THREE.AmbientLight(0xaaaaaa, 2.0);
// light.position.set(0, 20*r, 0);
// scene.add(light);

function image_board(imgpath, index){
    //plane
    var geometry = new THREE.BoxGeometry(w, h);
    var canvas = document.createElement("canvas");
    canvas.width = w;
    canvas.height = h;

    var texture = new THREE.TextureLoader().load(imgpath);
    //var img = new Image();
    // var ctx = canvas.getContext('2d');
    // ctx.translate(0.5, 0.5);
    // img.src = imgpath;
    // img.onload = function(){
    // 	var ctx = canvas.getContext('2d');
    //     ctx.drawImage(img, 0, 0);
    // 	ctx.font = "50px Arial";
    // 	ctx.fillStyle = "rgb(255, 0, 255)";
    // 	//var base = imgpath.split('/');
    // 	//var path = base[base.length-1];
    // 	//ctx.fillText(''+index, 10, 100); 
    // };
    // ctx.drawImage(img, 0, 0);
    // var texture = new THREE.Texture(canvas);
    texture.needsUpdate = true;
    texture.repeat.set(1,1);
    var material = new THREE.MeshBasicMaterial({map: texture, color: 0xFFFFFF});
    var obj = new THREE.Mesh(geometry, material);
    return obj;
}

var screens = [];
for(var i = 0; i < n; i++){
    var obj = image_board(imagelist[i], i)
    
    //solar like
    // obj.position.x = l*Math.cos(-theta*i);
    // obj.position.z = l*Math.sin(-theta*i);;
    // obj.rotation.y = theta*i;
    
    // var th = theta_offset - theta*i;
    // obj.position.x = r*Math.cos(-th);
    // obj.position.z = r*Math.sin(-th);
    // console.log('xz %d %d %s', obj.position.x, obj.position.z, imagelist[i]);
    //obj.rotation.y = Math.PI/2 + th;
    scene.add(obj);
    //add text
    screens.push(obj);
    // var num = "" + i;
    // var param = {
    //             size: 30, height: 4, curveSegments: 3,
    //             font: "helvetiker", weight: "bold", style: "normal",
    //             bevelThickness: 1, bevelSize: 2, bevelEnabled: true
    // }
    // var text = new THREE.TextGeometry(num, param);
    // text.position.x = r*Math.cos(-th);
    // text.position.z = r*Math.sin(-th);
    // //console.log('xz %d %d', obj.position.x, obj.position.z);
    // text.rotation.y = Math.PI/2 + th;
    // screens.push(text);

    // var geometry = new THREE.BoxGeometry(10, 10, 10);
    // var material = new THREE.MeshBasicMaterial({color: 0x00ff00});
    // var cube = new THREE.Mesh(geometry, material);
    // cube.position.x = r*Math.cos(-th);
    // cube.position.z = r*Math.sin(-th);
    // scene.add(cube);
}

//marker of unit vector
// var geometry = new THREE.BoxGeometry(50, 50, 50);
// var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
// var cube = new THREE.Mesh( geometry, material );
// cube.position.set(r/2, 0, 0);
// scene.add(cube);
// cube = new THREE.Mesh( geometry, material );
// cube.position.set(0, r/2, 0);
// scene.add(cube);
// cube = new THREE.Mesh( geometry, material );
// cube.position.set(0, 0, r/2);
// scene.add(cube);

//floor
// geometry = new THREE.BoxGeometry(2*r, -30, 2*r);
// var material = new THREE.MeshBasicMaterial( {color: 0x666666} );
// cube = new THREE.Mesh(geometry, material);
// cube.position.set(0, 0, 0);
// scene.add(cube);

scene.add(buildAxes(2*r));

//TODO: height, char size
camera.position.set(0, 0, 0.95*r);
control.target.set(0, 0, r);
camera.zoom = 50.0;
//camera.rotation.y = Math.PI;
//camera.lookAt(0, 0, -1000);

function render() {
    requestAnimationFrame(render);
    control.update();
    renderer.render(scene, camera);
    theta_offset += 0.002*theta;

    for(var i = 0; i < n; i++){
        var th = theta_offset - theta*i;
        var obj = screens[i];
        obj.position.x = r*Math.cos(-th);
        obj.position.z = r*Math.sin(-th);;
        obj.rotation.y = Math.PI/2 + th;
    }
}
render();
