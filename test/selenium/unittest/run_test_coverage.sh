#! /bin/sh
rm -rf result
mkdir result

pip3 install coverage

PYTHONPATH=./libs/ParametrizedTestCase/:./libs/HTMLTestRunner:$PYTHONPATH xvfb-run --server-args="-screen 0 1024x800x8" coverage run ./test_redmine.py --output=result/report.html

coverage html
