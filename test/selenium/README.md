Selenium test (with Chrome driver)
==================================

Prepare
-------
1. Put config data file named `.secret.json`

```
{
    "username": "USERNAME",
    "password": "PASSWORD",
    "redmine_url": "http://REDMINE/PATH"
}
```

2. download and install the latest chromedriver http://chromedriver.chromium.org/downloads
  * unzip and copy chromedriver to `/usr/local/bin`
3. install webdriver

```
brew update
brew install selenium-server-standalone
```

Run with unittest
------------------

```
cd unittest
./run_test.sh
```

Run with nose2
---------------

```
cd unittest
./run_test.sh
```

TODO
-----
* (unitest) bug: console output of test case is not separated
* alternative tool: pytest-html

3D test report
===============

Run
----
1. create capture list. `js/data.js` will be created.

```
./make_capture_list.py
```

2. serve 3d_report.html using web server and browse it.

e.g.

```
python3 -m http.server
```
open 3d_report.html

----
Takashi Masuyama < mamewotoko@gmail.com >
http://mamewo.ddo.jp/
