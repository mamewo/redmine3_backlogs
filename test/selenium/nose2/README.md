Redmine test report in 3D
==========================

Install lib
------------

```
npm i
```

Build
------------

1. prepare data (src/data.ml)
2. build

```
./build.sh
```

`src/m.js` is generated as javascript for browser.

Browse
------------

* browse report.html 

Source (src directory)
---------------------------

* Three.ml: threejs bindings
* main.ml: application
* data*.ml: data (imagepath, width, height)
  * use get_image_size.py to generate this file

Reference
---------

* [three.js](https://threejs.org/)
* [BuckleScript](https://bucklescript.github.io/)
* [w3reality/bs-three](https://github.com/w3reality/bs-three)

----
Takashi Masuyama < mamewotoko@gmail.com >  
https://mamewo.ddo.jp/
