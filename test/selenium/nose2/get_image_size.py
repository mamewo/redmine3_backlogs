#! /usr/bin/env python
from pymage_size import get_image_size
import sys


if __name__ == '__main__':
    print("let data = [|")
    for path in sys.argv[1:]:
        img_format = get_image_size(path)
        w, h = img_format.get_dimensions()
        print('("%s", %d, %d);' % (path, w, h))
    print("|]")
