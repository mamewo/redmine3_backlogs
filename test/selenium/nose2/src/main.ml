let _ = Js.log "Three.js BuckleScript binding r1"

open Three
(* open PromiseMonad *)

let pi = 2. *. asin 1.

(* navigator(browser) object *)
type document
type element = Dom.element

module rec Location:
sig
  class type _Location =
    object
      method href: string
      method search: string
    end
    type t = _Location Js.t
end = Location
external location: Location.t = "" [@@bs.val][@@bs.scope "window"]

module rec MouseEvent:
sig
  class type _MouseEvent =
    object
      method altKey: bool [@@bs.get]
      method button: int  [@@bs.get]
      method buttons: int Js.Array.t [@@bs.get]
      method clientX: int [@@bs.get]
      method clientY: int [@@bs.get]
      method ctrlKey: bool [@@bs.get]
      method metaKey: bool [@@bs.get]
      method movementX: int [@@bs.get]
      method movementY: int [@@bs.get]
      method movementX: int [@@bs.get]
      method movementY: int [@@bs.get]
      method offsetX: int [@@bs.get]
      method offsetY: int [@@bs.get]
      method pageX: int [@@bs.get]
      method pageY: int [@@bs.get]
      method region: string option [@@bs.get]
      (* method relatedTarget: string option [@@bs.get] *)
      method screenX: int [@@bs.get]
      method screenY: int [@@bs.get]
      method shiftKey: bool [@@bs.get]
           (* 0: no button
              1: left button   
              2: middle button
              3: right button *)
      method which: bool [@@bs.get]
           (*            mozInputSource
            * mozPressure *)
      method x: int [@@bs.get]
      method y: int [@@bs.get]
    end [@bs]
  type t = _MouseEvent Js.t
end = MouseEvent

(* 
https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent                                    *)                        
module rec KeyboardEvent:
sig
  class type _KeyboardEvent =
    object
      method getModifierState: int -> bool
      (* method initKeyState: 
       * method initKeyboardEvent:  *)
      method altKey: bool [@@bs.get]
      method char: string [@@bs.get]
      method charCode: int [@@bs.get]
      method code: string [@@bs.get]
      method ctrlKey: bool [@@bs.get]
      method isComposing: bool [@@bs.get]
      method key: string [@@bs.get]
      method keyCode: int [@@bs.get]
      (* method keyIdentifier: int [@@bs.get] *)
      (* method keyLocation: int [@@bs.get] *)
      method locale: string [@@bs.get]
      method location: int [@@bs.get]
      method metaKey: bool [@@bs.get]
      method repeat: bool [@@bs.get]
      method shiftKey: bool [@@bs.get]
      method which: int [@@bs.get] (* deperecated *)
    end [@bs]
  type t = _KeyboardEvent Js.t
  (* type key = int *)
end = KeyboardEvent
    
external document:document = "" [@@bs.val]
external getElementById: string -> element = "" [@@bs.val][@@bs.scope "document"]

external width:int = "window.innerWidth" [@@bs.val]
external height:int = "window.innerHeight" [@@bs.val]
external appendChild: element -> element -> unit = "" [@@bs.send]
external textContent: element -> string -> unit= "" [@@bs.set]
external createElement: document -> string -> unit -> element = "" [@@bs.send]
external requestAnimationFrame: (float -> unit) -> unit = "" [@@bs.val][@@bs.scope "window"]
external requestFullscreen: element -> unit = "" [@@bs.send]
(* external onresize: (unit -> unit) -> unit = "" [@@bs.set][@@bs.scope "window"] *)

external addEventListener: string -> (KeyboardEvent.t -> bool) -> bool -> unit = "" [@@bs.val][@@bs.scope "document"]    
external onclick: element -> (MouseEvent.t -> unit) -> unit = "" [@@bs.set]
                                                            
external onfullscreenchange: document -> (unit -> unit) -> unit = "" [@@bs.set]

let setSize = [%raw fun element width height -> "element.width = width; element.height = height;"]

let fov = 90.
let camera_distance h =
  h /. (2. *. tan (fov /. 2.)) +. 200.
            
type context = {
    scene: Scene.t;
    loader: TextureLoader.t;
    control: OrbitControls.t;
    camera: Camera.t;
    renderer: WebGLRenderer.t;
    angle: float ref;
    init_theta: float;
    obj_list: Group.t list ref;
    maxh: float ref;
  }

let random_theta = false
             
let image_board context image_path image_width image_height =
  let geometry = Geometry.Box.make image_width image_height 1 in
  let texture = context.loader##load image_path in
  let material = Material.MeshBasic.make [%bs.obj { color = 0xffffff; map = Some texture }] in
  texture##needsUpdate #= true;
  Mesh.make geometry material

let make_ring context image_array =
  let n = Array.length image_array in
  (* let theta = 2. *. pi /. (float_of_int n) in *)
  (* let image_width = 520 in
   * let image_height = 750 in *)
  (* let r = (float_of_int image_width) /. (2. *. sin(theta/. 2.)) in *)  
  (* let size_list = Array.map Imagelib.size image_array in *)
  let width_sum = Array.fold_left (fun x (_, y, _) -> x + y) 0 image_array in
  let r = (float_of_int width_sum) /. (2. *. pi) in
  let h = Array.fold_left (fun x (_, _, y) -> max x y) 0 image_array in
  let group = Group.make () in
  let current_theta = ref context.init_theta in
  for i = 0 to n - 1 do
    let (image_path, image_width, image_height) = Array.get image_array i in
    let obj = image_board context image_path image_width image_height in
    group##add obj;
    let delta_theta = asin((float_of_int image_width) /. (2. *. r)) in
    current_theta := !current_theta +. delta_theta;
    obj##position##x #= (r *. cos(!current_theta));
    obj##position##z #= (r *. sin(!current_theta));
    obj##rotation##y #= (pi /. 2. -. !current_theta);
    current_theta := !current_theta +. delta_theta;
  done;
  Js.Dict.set group##userData "r" r;
  Js.Dict.set group##userData "h" (float_of_int h);
  group

let rotate_all = true

let render context =
  begin
    if rotate_all then
      begin
        context.angle := !(context.angle) +. (pi /. (60. *. 800.));
        context.scene##rotation##y #= !(context.angle)
      end;
    context.renderer##render context.scene context.camera
  end

let rec animate context _ =
  context.control##update ();
  (* control_3d##update (); *)
  render context;
  requestAnimationFrame (animate context)

let make_context ?(theta=0.) ?(random_theta=false) canvas datalist =
  let init_theta =
    if random_theta then
      Random.float (2. *. pi)
    else
      theta in
  let ratio = (float_of_int width) /. (float_of_int height) in
  let param = [%bs.obj { antialias = true; canvas = canvas }] in
  let renderer = WebGLRenderer.make param in 
  let camera = Camera.Perspective.make ~fov ~aspect:ratio ~near:1. ~far:100000. in
  (* let control_3d = DeviceOrientationControls.make camera true in *)
  let control = OrbitControls.make camera renderer##domElement in
  let scene = Scene.make () in
  let loader = TextureLoader.make () in
  let angle = ref 0. in
  begin
    control##zoomSpeed = 5.;
    control##enableZoom = true;
    {
      scene = scene;
      loader = loader;
      control = control;
      camera = camera;
      renderer = renderer;
      angle = angle;
      init_theta = init_theta;
      obj_list = ref [];
      maxh = ref 0.;
    }
  end
  
let string_index str ch =
  let l = String.length str in
  let rec iter index = 
    if index >= l then
      raise Not_found
    else
      if String.get str index = ch then
        index
      else
        iter (index+1) in
  iter 0
  
let rec str_split ch str =
  if str = "" then
    []
  else
    try
      let pos = String.index str ch in
      let hd = String.sub str 0 pos in
      let tlstr = String.sub str (pos+1) ((String.length str)-pos-1) in
      let tl = str_split ch tlstr in
      hd::tl
    with Not_found ->
      [str]

let main ?(init_context=None) () =
  let init_theta =
    try
      let search = location##search in
      if Js.String.length search > 0 then
        let search2 = Js.String.substring ~from:1 ~to_:(Js.String.length search) search in
        let params2 = Js.String.split "&" search2 in
        let table = Js.Array.map (fun x -> let tmp = Js.String.split "=" x in (Js.Array.unsafe_get tmp 0, Js.Array.unsafe_get tmp 1)) params2 in
        match Js.Array.find (fun (x, _) -> x = "theta") table with
          Some (_, stheta) ->
           float_of_string stheta
        | None -> 0.
      else
        0.
    with _ -> 0. in
  let canvas =
    let tmp = getElementById "canvas" in
    setSize tmp width height;
    tmp in
  let datalist = [ Data.data ] in
  let context =
    match init_context with
      Some c -> c
    | None -> make_context ~theta:init_theta (Some canvas) datalist in
  let objlist = List.map (make_ring context) datalist in
  context.obj_list := objlist;
  let get_r obj =
    match Js.Dict.get obj##userData "r" with
      Some rvalue -> rvalue
    | None -> 7000. in
  (* let r =
   *   List.map get_r objlist |> List.fold_left min 1000000. in *)
  (* let control_3d = DeviceOrientationControls.make camera true in *)
  (* let effect = StereoEffect.make renderer in *)
  let y = ref 0. in
  let init_camera ?(index=0) context =
    let r = List.nth !(context.obj_list) index |> get_r in
    let y = ~-. !(context.maxh) *. (float_of_int index) in
    Js.log (Printf.sprintf "index, y, r %d %f %f\n" index y r);
    context.control##target##set 0.0 y r;
    context.camera##position##set 0.0 y (r -. (camera_distance (float_of_int  height))) in
  let keyhandler context e =
    let ch = e##key in
    let xdelta = 20. in
    let delta = pi /. 240.0 in
    let zoom_delta = 0.125 in 
    if ch = ">" then
      context.angle := !(context.angle) +. delta
    else if ch = "<" then
      context.angle := !(context.angle) -. delta
    else if ch = "]" then
      context.angle := !(context.angle) +. 3. *. delta
    else if ch = "[" then
      context.angle := !(context.angle) -. 3. *. delta
    else if ch = "-" then
      context.control##target##y #= (context.control##target##y -. 0.03 *. (float_of_int height))
    else if ch = "+" then
      context.control##target##y #= (context.control##target##y +. 0.03 *. (float_of_int height))
    else if ch = "}" then
      context.angle := !(context.angle) +. 9. *. delta
    else if ch = "{" then
      context.angle := !(context.angle) -. 9. *. delta
    else if List.exists (fun x -> ch = x) ["0"; "1"; "2"; "3"; "4"; "5"; "6"; "7"; "8"; "9"] then
      let index = (int_of_string ch) mod List.length !(context.obj_list) in
      init_camera ~index context
    else if ch = "t" then
      context.control##enabled #= (not (context.control##enabled))
    else if ch = "r" then
      context.angle := Random.float (2. *. pi)
    else if ch = "a" then
      context.camera##position##x #= (context.camera##position##x +. xdelta)
    else if ch = "d" then
      context.camera##position##x #= (context.camera##position##x -. xdelta)
    else if ch = "w" then
      context.camera##position##z #= (context.camera##position##z +. xdelta)
    else if ch = "s" then
      context.camera##position##z #= (context.camera##position##z -. xdelta)
    else if ch = "e" then
      begin
        context.camera##position##y #= (context.camera##position##y +. xdelta);
        context.control##target##y #= (context.control##target##y +. xdelta)
      end
    else if ch = "q" then
      begin
        context.camera##position##y #= (context.camera##position##y -. xdelta);
        context.control##target##y #= (context.control##target##y -. xdelta)
      end
    else if ch = "R" then
      init_camera context
    else
      ();
    true in
  let maxh =
    List.fold_left (fun x y ->
        match Js.Dict.get y##userData "h" with
          Some rvalue -> max x rvalue
        | None -> x)
      0.
      !(context.obj_list) in
  begin
    context.maxh := maxh;
    addEventListener "keydown" (keyhandler context) false;
    context.renderer##setSize width height;
    (* effect##setSize width height; *)
    (* control_3d##connect (); *)
    (* appendChild mainDiv context.renderer##domElement; *)
  
    List.iter (fun x -> x##position##y #= !y;
                          y := !y -. maxh;
                          context.scene##add x)
      !(context.obj_list);
    init_camera context;
    animate context 0.0;
  end

(* click body: start 3d in full screen mode*)
let init_fullscreen canvas =
  let fullscreen _ =
    requestFullscreen canvas in
  begin
    onclick canvas fullscreen;
    onfullscreenchange document main
  end

let _ =
  let canvas = getElementById "canvas" in
  (* onresize (fun () -> setSize canvas width height; main ()); *)
  begin
    Random.self_init ();
    init_fullscreen canvas
  end
