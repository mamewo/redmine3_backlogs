#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from enum import Enum
import json
import os
import os.path
import shutil
import time
import unittest
from urllib.parse import urljoin
from parameterized import parameterized_class

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import logging

logger = logging.getLogger('test.redmine')

SCREENSHOT_DIRNAME = 'screenshot'
SCREENSHOT_DIR = None

# prepare .secret.json like below
#
# { 'username': 'xxx',
#   'password': 'xxx',
#   'redmine_url': 'xxx'
#   'project_name': 'xxx' }

USERNAME = None
PASSWORD = None
REDMINE_URL_BASE = None
PROJECT_NAME = None


def small_test(func):
    func.small_test = True
    return func


def micro_test(func):
    func.micro_test = True
    return func

class DeviceType(Enum):
    DESKTOP = 1
    MOBILE = 2

@parameterized_class(('REDMINE_SUBDIR', 'device_type'), [
    ('redmine', DeviceType.DESKTOP), # farend_fancy
    ('redmine', DeviceType.MOBILE), # farend_fancy
    ('redmine_gitmike', DeviceType.DESKTOP),
    ('redmine_modula_gitlab', DeviceType.DESKTOP),
    ('redmine_themes', DeviceType.DESKTOP),
    ('redmine_classic', DeviceType.DESKTOP),
    ('redmine_default', DeviceType.DESKTOP),
])
class TestRedmineBrowseCase(unittest.TestCase):
    """ browse page and take screenshot."""
   
    def start(self):
        global USERNAME
        global PASSWORD
        global REDMINE_URL_BASE
        global PROJECT_NAME
        global SCREENSHOT_DIR
        global SCREENSHOT_DIRNAME
        
        USERNAME = os.environ.get('REDMINE_USERNAME')
        PASSWORD = os.environ.get('REDMINE_PASSWORD')
        PROJECT_NAME = os.environ.get('REDMINE_PROJECT_NAME')
        REDMINE_URL_BASE = os.environ.get('REDMINE_URL_BASE')
        # if not parameterized_class
        if not hasattr(self, 'REDMINE_SUBDIR'):
            self.REDMINE_SUBDIR = 'redmine'
        if not hasattr(self, 'device_type'):
            self.device_type = DeviceType.DESKTOP
        output_dir = 'result'
        SCREENSHOT_DIR = os.path.join(output_dir, SCREENSHOT_DIRNAME)
        if None in [USERNAME, PASSWORD, PROJECT_NAME]:
            raise Exception('not initialized %s' % [USERNAME, PASSWORD, PROJECT_NAME])
    
    def setUp(self):
        self.start()
        user_agent = "Mozilla/5.0 (Linux; Android 4.1.2; SHL21 Build/S4011) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.83 Mobile Safari/537.36"
        # destkop
        if self.device_type == DeviceType.DESKTOP:
            dim = (1024, 800)
        else:
            dim = (600, 1024)

        window_size = ",".join(map(str, dim))
        logger.debug("window size: %s" % window_size)
        opts = Options()
        opts.add_argument("--user-agent=" + user_agent)
        opts.add_argument("--window-size=" + window_size)
        self.driver = webdriver.Chrome(chrome_options=opts)
        self.screenshot = []

    def tearDown(self):
        self.driver.close()
        self.driver.quit()

    def save_screenshot(self, name=None):
        if name is None:
            name = self._testMethodName
        filename = name + '.png'
        time_prefix = int(time.time())
        pid = os.getpid()
        basename = '%d_%d_%s' % (time_prefix, pid, filename)
        htmlpath = "%s/%s" % (SCREENSHOT_DIRNAME, basename)
        ospath = "%s/%s" % (SCREENSHOT_DIR, basename)
        self.driver.save_screenshot(ospath)
        self.screenshot.append({'path': htmlpath,
                                'url': self.driver.current_url})
        logger.info("""<div><a href="{1}">{1}</a><br><a href="{0}"><img class="screen_capture" src="{0}"></a></div>""".format(htmlpath, self.driver.current_url))

    def get_page(self, path):
        global REDMINE_URL_BASE
        url = urljoin(REDMINE_URL_BASE, self.REDMINE_SUBDIR + '/')
        url = urljoin(url, path)
        logger.info(url)
        self.driver.get(url)
        time.sleep(3)      
        
    def login(self):
        self.get_page('login')
        self.save_screenshot()

        if self.device_type == DeviceType.DESKTOP:
            login_link = self.driver.find_element_by_xpath('//*[@id="account"]/ul/li[1]/a')
            login_link.click()
            time.sleep(3)
        username_text = self.driver.find_element_by_xpath('//*[@id="username"]')
        username_text.click()
        username_text.clear()
        username_text.send_keys(USERNAME)
        password_text = self.driver.find_element_by_xpath('//*[@id="password"]')
        password_text.click()
        password_text.clear()
        password_text.send_keys(PASSWORD)

        login_button = self.driver.find_element_by_xpath('//*[@id="login-submit"]')
        login_button.click()
        time.sleep(3)

        self.save_screenshot()

    @small_test
    def test_login(self):
        self.login()
        # TODO: add assert

    def test_admin_users(self):
        self.login()
        self.get_page('users')
        self.save_screenshot()

    def test_activity(self):
        self.login()
        self.get_page('projects/%s/activity' % PROJECT_NAME)
        self.save_screenshot()

    def test_roadmap(self):
        self.login()
        self.get_page('projects/%s/roadmap' % PROJECT_NAME)
        self.save_screenshot()
        
    @small_test
    def test_backlog(self):
        self.login()
        self.get_page('rb/master_backlog/%s' % PROJECT_NAME)
        self.save_screenshot()

    def test_release(self):
        self.login()
        self.get_page('rb/releases/%s' % PROJECT_NAME)
        self.save_screenshot()

    @small_test
    def test_ticket(self):
        self.login()
        self.get_page('projects/%s/issues' % PROJECT_NAME)
        self.save_screenshot()
        
    @small_test
    def test_new_ticket(self):
        self.login()
        self.get_page('projects/%s/issues/new' % PROJECT_NAME)
        self.save_screenshot()

    def test_time_entries(self):
        self.login()
        self.get_page('projects/%s/time_entries' % PROJECT_NAME)
        self.save_screenshot()
        
    @small_test
    def test_gantt(self):
        self.login()
        self.get_page('projects/%s/issues/gantt' % PROJECT_NAME)
        self.save_screenshot()

    @small_test
    def test_calendar(self):
        self.login()
        self.get_page('projects/%s/issues/calendar' % PROJECT_NAME)
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_news(self):
        self.login()
        self.get_page('projects/%s/news' % PROJECT_NAME)
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_documents(self):
        self.login()
        self.get_page('projects/%s/documents' % PROJECT_NAME)
        self.save_screenshot()

    @micro_test
    def test_wiki(self):
        self.login()
        self.get_page('projects/%s/wiki' % PROJECT_NAME)
        # self.save_screenshot()
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_files(self):
        self.login()
        self.get_page('projects/%s/files' % PROJECT_NAME)
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_glossary(self):
        self.login()
        self.get_page('projects/%s/glossary' % PROJECT_NAME)
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_setting(self):
        self.login()
        self.get_page('projects/%s/settings' % PROJECT_NAME)
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_setting_modules(self):
        self.login()
        self.get_page('projects/%s/settings/modules' % PROJECT_NAME)
        self.save_screenshot()

    # @unittest.skip("tmp")
    def test_setting_members(self):
        self.login()
        self.get_page('projects/%s/settings/members' % PROJECT_NAME)
        self.save_screenshot()

    def test_admin_projects(self):
        self.login()
        self.get_page('admin/projects')
        self.save_screenshot()

    def test_admin_groups(self):
        self.login()
        self.get_page('groups')
        self.save_screenshot()

    def test_admin_roles(self):
        self.login()
        self.get_page('roles')
        self.save_screenshot()

    def test_admin_trackers(self):
        self.login()
        self.get_page('trackers')
        self.save_screenshot()

    def test_admin_ticket_statuses(self):
        self.login()
        self.get_page('issue_statuses')
        self.save_screenshot()

    @small_test
    def test_admin_kanban(self):
        self.login()
        self.get_page('rb/taskboards/2')
        self.save_screenshot()

    def test_admin_workflows(self):
        self.login()
        self.get_page('workflows/edit')
        self.save_screenshot()

    def test_admin_custom_fields(self):
        self.login()
        self.get_page('custom_fields')
        self.save_screenshot()

    def test_admin_enumerations(self):
        self.login()
        self.get_page('enumerations')
        self.save_screenshot()

    def test_admin_settings(self):
        self.login()
        self.get_page('settings')
        self.save_screenshot()
        # TODO: test tabs (display...)

    def test_admin_ldap(self):
        self.login()
        self.get_page('auth_sources')
        self.save_screenshot()

    def test_admin_global_issue_templates(self):
        self.login()
        self.get_page('global_issue_templates')
        self.save_screenshot()

    def test_admin_version(self):
        self.login()
        self.get_page('admin/info')
        self.save_screenshot()

    def test_admin_plugin(self):
        self.login()
        self.get_page('admin/plugins')
        self.save_screenshot()

    def test_my_page(self):
        self.login()
        self.get_page('my/page')
        self.save_screenshot()

    def test_private_config(self):
        self.login()
        self.get_page('my/account')
        self.save_screenshot()

    @small_test
    def test_first_ticket(self):
        self.login()
        self.get_page('issues/1')
        self.save_screenshot()
