#! /bin/sh
HEADLESS=yes

# Option
# -A small_test: run small_test
#TARGET=test_redmine.py:TestRedmineBrowseCase.test_wiki

rm -rf result
mkdir -p result/screenshot
#export REDMINE_USERNAME=
#export REDMINE_PASSWORD=
#export REDMINE_PROJECT_NAME=
#export REDMINE_URL=

if [ -f ../env ]; then
    . ../env
fi
. ./venv/bin/activate

UNAME=$(uname -s)
TARGET=test_redmine
PREFIX_COMMAND=

if [ "$UNAME" = "Linux" ] && [ "$HEADLESS" = "yes" ]; then
    # nose2
    xvfb-run -a --server-args="-screen 0 1024x1024x8" nose2 -c nose2.cfg $@ $TARGET
else
    nose2 -c nose2.cfg $@ $TARGET
fi
