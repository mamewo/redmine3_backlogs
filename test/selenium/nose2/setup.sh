#! /bin/bash
# Ubuntu, Debian
# sudo apt-get update && sudo apt-get install -y xvfb python3-pip python3-venv

if [ -d venf ]; then
    echo venv already exists
    exit 0
fi

python3 -m venv venv
# ( cd venv/lib64/python3.5/; pwd ; patch -p3 < ../../../patch/venv.patch )
# linux
. ./venv/bin/activate
pip3 install wheel
pip3 install -r requirements.txt
( cd venv/lib/python*/site-packages/; pwd ; patch -p1 < ../../../../patch/nose2_html_report.patch && cp ../../../../patch/report.html nose2_html_report/templates/ )
