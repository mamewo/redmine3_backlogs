#! /bin/sh
set -x

if [ ! mariadb_data/volume.tar.gz ]; then
    echo backup file is not found, skip restore 
    exit 0
fi

sudo tar xvfz mariadb_data/volume.tar.gz volume/mariadb/backup
sudo tar xvfz mariadb_data/volume.tar.gz volume/redmine/files

sudo cp drop_tag.sql volume/mariadb/backup
sleep 30
docker-compose -f docker-compose-mariadb.yml exec -T db ss -lnp
docker-compose -f docker-compose-mariadb.yml exec -T db ps auxww
docker-compose -f docker-compose-mariadb.yml exec -T db sh -c "mysql -u root -pexample --protocol tcp -h localhost --port 3306 -D redmine < /var/lib/mysql/backup/redmine_mariadb.sql"
# docker-compose exec db sh -c "mysql -u root -pexample -D redmine < /var/lib/mysql/backup/select.sql"
docker-compose -f docker-compose-mariadb.yml exec -T db sh -c "mysql -u root -pexample --protocol tcp -h localhost --port 3306 -D redmine < /var/lib/mysql/backup/drop_tag.sql"
