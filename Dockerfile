FROM library/redmine:4.1.1
# http://www.redmine.org/issues/26637

RUN apt-get update && apt-get install -y --fix-missing --no-install-recommends \
  git gcc make g++ mercurial bzr subversion patch cvs darcs imagemagick \
  vim less \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY redmine/install.sh /etc
COPY redmine/config3.diff /etc
COPY redmine/relate_title.patch /etc
COPY redmine/config.ru /usr/src/redmine/
COPY redmine/color.patch /etc
COPY redmine/26637.patch /etc
COPY redmine/rails5.patch /etc
COPY redmine/rails5.patch /etc
COPY redmine/application.css.patch /etc

# theme
COPY ./lib/redmine/themes/farend_fancy /usr/src/redmine/public/themes/
COPY ./lib/redmine/themes/gitmike /usr/src/redmine/public/themes/
COPY ./lib/redmine/themes/modula-gitlab /usr/src/redmine/public/themes/
COPY ./lib/redmine/themes/redmine-themes /usr/src/redmine/public/themes/

RUN patch -p1 < /etc/config3.diff
#RUN patch -p1 < /etc/relate_title.patch
# RUN patch -p1 < /etc/color.patch
RUN patch -p1 < /etc/26637.patch
RUN mkdir -pm 755 /usr/local/bundle/cache /usr/local/bundle/gems /home/redmine \
  && chown -R redmine.redmine /usr/local/bundle/ /home/redmine
RUN patch -p0 < /etc/rails5.patch
RUN patch -p0 < /etc/application.css.patch

RUN wget https://github.com/jgm/pandoc/releases/download/2.5/pandoc-2.5-1-amd64.deb \
  && dpkg -i pandoc-2.5-1-amd64.deb \
  && rm pandoc-2.5-1-amd64.deb

# copy plugins
COPY ./lib/redmine/plugins/redmine_holidays_plugin /usr/src/redmine/plugins/redmine_holidays_plugin
COPY ./lib/redmine/plugins/clipboard_image_paste /usr/src/redmine/plugins/clipboard_image_paste
COPY ./lib/redmine/plugins/redmine_local_avatars /usr/src/redmine/plugins/redmine_local_avatars
COPY ./lib/redmine/plugins/redmine_tagging /usr/src/redmine/plugins/redmine_tagging
COPY ./lib/redmine/plugins/redmine_backlogs /usr/src/redmine/plugins/redmine_backlogs
COPY ./lib/redmine/plugins/redmine_pandoc_formatter /usr/src/redmine/plugins/redmine_pandoc_formatter
COPY ./lib/redmine/plugins/redmine_custom_css /usr/src/redmine/plugins/redmine_custom_css
COPY ./lib/redmine/plugins/redmine_issue_templates /usr/src/redmine/plugins/redmine_issue_templates
COPY ./lib/redmine/plugins/sidebar_hide /usr/src/redmine/plugins/sidebar_hide
COPY ./lib/redmine/plugins/redmine_aceeditor /usr/src/redmine/plugins/redmine_aceeditor
#COPY ./lib/redmine/plugins/view_customize /usr/src/redmine/plugins/view_customize
COPY ./lib/redmine/themes/farend_fancy /usr/src/redmine/public/themes/farend_fancy
